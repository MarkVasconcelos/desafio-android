package br.rgb.dribbblereaderapp.lang;

public interface Listener<T> {
    void on(T evt);
}

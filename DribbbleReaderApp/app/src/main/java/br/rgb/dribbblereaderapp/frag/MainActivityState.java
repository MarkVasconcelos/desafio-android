package br.rgb.dribbblereaderapp.frag;

import android.support.v4.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import br.rgb.dribbblereaderapp.model.Shot;
import br.rgb.dribbblereaderapp.web.DribbbleAPISchema;

/**
 * The State fragment is viewless
 */
public class MainActivityState extends Fragment {
    private List<Shot> data = new ArrayList<>();
    private int loadedPages;
    private DataSort sort = DataSort.DEFAULT;
    private int currentView;
    private int viewDataAt;

    public MainActivityState(){
        setRetainInstance(true);
    }

    public void setLoadedPages(int loadedPages) {
        this.loadedPages = loadedPages;
    }

    public void addData(List<Shot> result) {
        data.addAll(result);
    }

    public List<Shot> getLoadedData() {
        return data;
    }

    public void setViewDataAt(int viewDataAt) {
        this.viewDataAt = viewDataAt;
    }

    public int getViewDataAt() {
        return viewDataAt;
    }

    public int getLoadedPages(){ return loadedPages; }

    public DataSort getSort() {
        return sort;
    }

    public void setSort(DataSort sort) {
        this.sort = sort;
    }

    public enum DataSort {
        DEFAULT("Popularidade", ""), COMMENTS("Comentarios", "comments"), RECENTS("Data", "recent"), VIEWS("Visualizações", "views");

        public String name;
        private String paramValue;

        DataSort(String name, String paramValue){
            this.name = name;
            this.paramValue = paramValue;
        }

        public String getSortParam(String append){
            if(this == DEFAULT)
                return "";

            return append + DribbbleAPISchema.sortParam(paramValue);
        }
    }
}

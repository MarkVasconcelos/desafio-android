package br.rgb.dribbblereaderapp.web;

public class DribbbleAPISchema {
    private static final String API_KEY = "aac3b871c0685bcc3d426afd422ab7b1ad45e039401862794f05941d9d880fad";
    private static final String ENDPOINT = "https://api.dribbble.com/v1/";


    public static String getShotsUrl(int page) {
        String url = ENDPOINT + "shots";
        url += "?" + apiKeyParameter();
        if(page != 0)
            url += "&page=" + page;

        return url;
    }

    private static String apiKeyParameter() {
        return "access_token=" + API_KEY;
    }

    public static String sortParam(String paramValue) {
        return "sort="+paramValue;
    }
}

package br.rgb.dribbblereaderapp.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.rgb.dribbblereaderapp.R;
import br.rgb.dribbblereaderapp.model.Shot;

class ShotItemView extends FrameLayout {
    private final ImageView preview;
    private final TextView title, summary, from;

    public ShotItemView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.view_shot_item, this, true);

        preview = (ImageView) findViewById(R.id.iv_preview);
        title = (TextView) findViewById(R.id.tv_name);
        summary = (TextView) findViewById(R.id.tv_summary);
        from = (TextView) findViewById(R.id.tv_from);
    }

    public void setObject(Shot shot) {
        Picasso.with(getContext()).load(shot.getImages().getNormal()).into(preview);
        title.setText(shot.getTitle());
        summary.setText(shot.getSummary());
        from.setText(shot.getFrom());
    }
}

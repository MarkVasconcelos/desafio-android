package br.rgb.dribbblereaderapp.presenter;

import android.content.Context;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import br.rgb.dribbblereaderapp.frag.MainActivityState;
import br.rgb.dribbblereaderapp.model.Shot;

public interface MainActivityPresentatationContract {
    interface Listener {
        void onCreate(Bundle savedInstanceState);
        void onSaveInstanceState(Bundle icicle);
        void onOptionMenuItemClicked(MenuItem opt);
        void onDataScrollEnded();
        void onPrepareOptionsMenu(Menu menu);
        void onRestoreInstanceState(Bundle savedInstanceState);
    }

    interface View {
        MainActivityState getState();
        Context context();
        int getCurrentItemVisiblePosition();
        Menu getMenu();
        void showList(List<Shot> loadedData, int viewDataAt, boolean scrollToPosition);
        void showLoading();
        void closeLoading();
    }
}

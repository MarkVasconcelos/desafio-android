package br.rgb.dribbblereaderapp.web;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import br.rgb.dribbblereaderapp.model.Shot;

public class DribbbleParser {
    public static List<Shot> parseShots(String getShotsResult) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssz")
                .create();
        return gson.fromJson(getShotsResult, new TypeToken<List<Shot>>(){}.getType());
    }
}

package br.rgb.dribbblereaderapp.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.rgb.dribbblereaderapp.lang.Listener;
import br.rgb.dribbblereaderapp.model.Shot;

public class ShotAdapter extends RecyclerView.Adapter {
    private final List<Shot> data = new ArrayList<>();
    private final Listener<Void> onDataScrollEndedListener;

    public ShotAdapter(Listener<Void> onDataScrollEndedListener) {
        this.onDataScrollEndedListener = onDataScrollEndedListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewViewHolder(new ShotItemView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position == data.size() - 1)
            onDataScrollEndedListener.on(null);
        ((ShotItemView)holder.itemView).setObject(data.get(position));
    }

    @Override public int getItemCount() { return data.size(); }

    public void setData(List<Shot> loadedData) {
        data.clear();
        data.addAll(loadedData);
        notifyDataSetChanged();
    }

    class ViewViewHolder extends RecyclerView.ViewHolder {
        public ViewViewHolder(View itemView) {
            super(itemView);
            itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }
    }
}

package br.rgb.dribbblereaderapp.model;

public class Images {
    private String normal;

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }
}

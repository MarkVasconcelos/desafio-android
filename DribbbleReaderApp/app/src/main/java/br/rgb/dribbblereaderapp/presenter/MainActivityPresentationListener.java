package br.rgb.dribbblereaderapp.presenter;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import br.rgb.dribbblereaderapp.R;
import br.rgb.dribbblereaderapp.WebRequestService;
import br.rgb.dribbblereaderapp.frag.MainActivityState;
import br.rgb.dribbblereaderapp.lang.Listener;
import br.rgb.dribbblereaderapp.model.Shot;
import br.rgb.dribbblereaderapp.web.DribbbleAPISchema;
import br.rgb.dribbblereaderapp.web.DribbbleParser;

public class MainActivityPresentationListener implements MainActivityPresentatationContract.Listener {
    private final MainActivityPresentatationContract.View view;
    private MainActivityState state;
    private final OnResultReceivedListener listener = new OnResultReceivedListener();

    public MainActivityPresentationListener(MainActivityPresentatationContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        state = view.getState();

        view.showList(state.getLoadedData(), state.getViewDataAt(), true);

        if(savedInstanceState != null)
            request = savedInstanceState.getParcelable(REQUEST_KEY);

        if(state.getLoadedData().isEmpty()) {
            view.showLoading();

            if(request == null) {
                request = new LoadListReceiver(new Handler(), 1);
                WebRequestService.request(view.context(), DribbbleAPISchema.getShotsUrl(1) + state.getSort().getSortParam("&"), request);
            }

            request.onResultReceivedListener = listener;
        } else if (request != null){
            request.onResultReceivedListener = listener;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.opt_sort_by).setTitle(state.getSort().name);
    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {
        icicle.putParcelable(REQUEST_KEY, request);
        state.setViewDataAt(view.getCurrentItemVisiblePosition());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
//        onPrepareOptionsMenu(view.getMenu());
    }

    @Override
    public void onOptionMenuItemClicked(MenuItem opt) {
        if(opt.getItemId() == R.id.opt_sort_by){
            /**
             * Sub-Menu item id
             */
            return;
        }
        if(opt.getItemId() == R.id.opt_pop){
            state.setSort(MainActivityState.DataSort.DEFAULT);
        } else if(opt.getItemId() == R.id.opt_comm){
            state.setSort(MainActivityState.DataSort.COMMENTS);
        } else if(opt.getItemId() == R.id.opt_date){
            state.setSort(MainActivityState.DataSort.RECENTS);
        } else if(opt.getItemId() == R.id.opt_vis){
            state.setSort(MainActivityState.DataSort.VIEWS);
        }

        onPrepareOptionsMenu(view.getMenu());
        /**
         * Currently, only sort items are displayed as options, so its fine to start loading again here
         */
        state.getLoadedData().clear();
        state.setLoadedPages(0);

        view.showLoading();
        request = new LoadListReceiver(new Handler(), 1);
        WebRequestService.request(view.context(), DribbbleAPISchema.getShotsUrl(1) + state.getSort().getSortParam("&"), request);
        request.onResultReceivedListener = listener;
    }

    @Override
    public void onDataScrollEnded() {
        request = new LoadListReceiver(new Handler(), state.getLoadedPages() + 1);
        request.onResultReceivedListener = listener;
        WebRequestService.request(view.context(), DribbbleAPISchema.getShotsUrl(state.getLoadedPages() + 1) + state.getSort().getSortParam("&"), request);
    }

    /**
     * This class is bound to the instance
     */
    class OnResultReceivedListener implements Listener<LoadListReceiver> {
        @Override
        public void on(LoadListReceiver evt) {
            if(evt.page == 1)
                view.closeLoading();

            request.onResultReceivedListener = null;
            request = null;

            state.addData(evt.result);
            state.setLoadedPages(evt.page);
            view.showList(state.getLoadedData(), state.getViewDataAt(), false);
        }
    };

    private static final String REQUEST_KEY = "request_key";
    private LoadListReceiver request;

    /**
     * Class intended to persist trough context changes
     */
    private static class LoadListReceiver extends ResultReceiver {
        private Listener<LoadListReceiver> onResultReceivedListener;
        private int page;
        private List<Shot> result;

        public LoadListReceiver(Handler handler, int page) {
            super(handler);
            this.page = page;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if(onResultReceivedListener == null)
                return;

            String receivedResult = WebRequestService.getResult(resultData);
            result = DribbbleParser.parseShots(receivedResult);
            onResultReceivedListener.on(this);
        }
    }
}

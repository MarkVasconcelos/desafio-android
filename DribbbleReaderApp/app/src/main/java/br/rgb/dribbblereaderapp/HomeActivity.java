package br.rgb.dribbblereaderapp;

import android.content.Context;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import br.rgb.dribbblereaderapp.frag.MainActivityState;
import br.rgb.dribbblereaderapp.lang.Listener;
import br.rgb.dribbblereaderapp.model.Shot;
import br.rgb.dribbblereaderapp.presenter.MainActivityPresentatationContract;
import br.rgb.dribbblereaderapp.presenter.MainActivityPresentationListener;
import br.rgb.dribbblereaderapp.view.ShotAdapter;

public class HomeActivity extends AppCompatActivity implements MainActivityPresentatationContract.View {
    private MainActivityPresentatationContract.Listener listener = new MainActivityPresentationListener(this);
    private RecyclerView data;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        data = (RecyclerView) findViewById(R.id.data);
        data.setLayoutManager(new LinearLayoutManager(this));
        data.setAdapter(new ShotAdapter(new Listener<Void>(){
            @Override
            public void on(Void evt) {
                listener.onDataScrollEnded();
            }
        }));

        listener.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        listener.onPrepareOptionsMenu(menu);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        listener.onOptionMenuItemClicked(item);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        listener.onSaveInstanceState(outState);
    }

    private static final String WORKER_TAG = "worker_tag_key";
    @Override
    public MainActivityState getState() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentByTag(WORKER_TAG);
        if(f != null)
            return (MainActivityState) f;

        fm.beginTransaction().add(f = new MainActivityState(), WORKER_TAG).commit();
        return (MainActivityState) f;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        listener.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public Context context() {
        return this;
    }

    @Override
    public int getCurrentItemVisiblePosition() {
        return ((LinearLayoutManager) data.getLayoutManager()).findFirstVisibleItemPosition();
    }

    @Override
    public Menu getMenu() {
        return menu;
    }

    @Override
    public void showList(List<Shot> loadedData, int viewDataAt, boolean scrollToPosition) {
        ((ShotAdapter) data.getAdapter()).setData(loadedData);
        if(scrollToPosition)
            data.scrollToPosition(viewDataAt);
    }

    @Override
    public void showLoading() {
        LoadingActivity.openFrom(this);
    }

    @Override
    public void closeLoading() {
        LoadingActivity.closeFrom(this);
    }
}

package br.rgb.dribbblereaderapp;

import org.junit.Test;

import java.util.List;

import br.rgb.dribbblereaderapp.model.Shot;
import br.rgb.dribbblereaderapp.web.DribbbleParser;

import static org.junit.Assert.*;

/**
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ServiceUnitTest {
    static final String GET_SHOTS_RESULT;
    static {
        StringBuilder xmlSample = new StringBuilder();
        xmlSample.append("[");
        xmlSample.append("{");
        xmlSample.append("\"title\": \"Fold animation\",");
        xmlSample.append("\"description\": \"<p>Just a practice on spilt fold design animation. Hope you like it. Tools : Sketch, Illustrator, Photoshop, After Effects\n<br />Image credits : <a href=\\\"https://www.shutterstock.com/g/msmoloko\\\" rel=\\\"nofollow noreferrer\\\">msmoloko</a> &amp;  <a href=\\\"https://unsplash.com/\\\" rel=\\\"nofollow noreferrer\\\">unsplash</a></p>\n\n<p>Check attached <a href=\\\"https://cdn.dribbble.com/users/692322/screenshots/3443280/attachments/755363/comp_1_7.mp4_9.07.40_pm.mp4\\\" rel=\\\"nofollow noreferrer\\\">video</a> for HD Experience.\n</p>\",");
        xmlSample.append("\"images\": {");
        xmlSample.append("\"normal\": \"https://cdn.dribbble.com/users/692322/screenshots/3443280/anime_1x.gif\"");
        xmlSample.append("},");
        xmlSample.append("\"views_count\": 3417,");
        xmlSample.append("\"likes_count\": 337,");
        xmlSample.append("\"comments_count\": 24,");
        xmlSample.append("\"created_at\": \"2017-04-19T06:45:26Z\",");
        xmlSample.append("\"html_url\": \"https://dribbble.com/shots/3443280-Fold-animation\",");
        xmlSample.append("\"tags\": [");
        xmlSample.append("\"blog\", \"cards\", \"design\", \"flower\", \"landing page\", \"minimal\", \"mobile\", \"presentation\", \"shapes\", \"split fold  uidesign\", \"webdesign\"");
        xmlSample.append("],");
        xmlSample.append("\"user\": {");
        xmlSample.append("\"name\": \"Divan\"");
        xmlSample.append("}");
        xmlSample.append("}");
        xmlSample.append("]");

        GET_SHOTS_RESULT = xmlSample.toString();
    }
    @Test
    public void parseIsCorrect() throws Exception {
        List<Shot> list = DribbbleParser.parseShots(GET_SHOTS_RESULT);
        assertEquals(1, list.size());

        Shot result = list.get(0);
        assertEquals(11, result.getTags().length);
        assertEquals("Divan", result.getUser().getName());
        assertEquals("https://cdn.dribbble.com/users/692322/screenshots/3443280/anime_1x.gif", result.getImages().getNormal());
    }
}